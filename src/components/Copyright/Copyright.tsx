import './Copyright.css';

import { cn } from '@bem-react/classname';
import * as React from 'react';

const block = cn('Copyright');

export const Copyright: React.FC<{}> = () => <div className={block()}>© 2019 ampmbureau</div>;
