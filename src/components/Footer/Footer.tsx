import './Footer.css';

import { cn } from '@bem-react/classname';
import { classnames } from '@bem-react/classnames';
import React, { useContext } from 'react';

import arrowUpIcon from '../../../assets/icons/arrow-up.svg';
import facebookIcon from '../../../assets/icons/facebook-icon.svg';
import instagramIcon from '../../../assets/icons/instagram-icon.svg';
import vkIcon from '../../../assets/icons/vkontakte-icon.svg';
import { LocaleContext } from '../../App';
import { scrollToTop } from '../../service/scroll';
import { Button } from '../Button/Button';
import { Copyright } from '../Copyright/Copyright';
import { SocialLink, SocialLinkProps } from '../SocialLink/SocialLink';

const block = cn('Footer');

type FooterProps = {
  className: string;
};

const links: (Omit<SocialLinkProps, 'altText'> & { name: string })[] = [
  {
    name: 'facebook',
    imgSrc: facebookIcon as string,
    href: 'https://www.facebook.com/ampmbureau',
    customIconStyle: {
      width: '40px',
      marginTop: '-2px'
    }
  },
  {
    name: 'vkontakte',
    imgSrc: vkIcon,
    href: 'https://vk.com/ampmbureau',
    customIconStyle: {
      width: '40px',
      marginBottom: '-6px'
    }
  },
  {
    name: 'instagram',
    imgSrc: instagramIcon,
    href: 'https://www.instagram.com/ampmbureau',
    customIconStyle: {
      width: '42px',
      marginLeft: '6px',
      marginBottom: '-2px'
    }
  }
];

export const Footer: React.FC<FooterProps> = ({ className }) => {
  const { i18n } = useContext(LocaleContext);

  function renderScrollTopButton() {
    return (
      <div className={block('ScrollTopButtonContainer')}>
        <Button className={block('ScrollTopButton')} onClick={scrollToTop}>
          <img src={arrowUpIcon} alt={i18n('toTheTop')} />
        </Button>
      </div>
    );
  }

  return (
    <div className={classnames(block(), className)}>
      <div className={classnames(block('Side'), block('Copyright'))}>
        <Copyright />
      </div>
      <div className={block('Center')}>
        <ul className={block('Contacts')}>
          {links.map(item => (
            <li key={item.name}>
              <SocialLink
                altText={i18n(`${item.name}Link`)}
                imgSrc={item.imgSrc}
                href={item.href}
                customIconStyle={item.customIconStyle}
              />
            </li>
          ))}
        </ul>
      </div>
      <div className={block('Side')}>{renderScrollTopButton()}</div>
    </div>
  );
};
