import './Logo.css';

import { cn } from '@bem-react/classname';
import { classnames } from '@bem-react/classnames';
import React, { useContext } from 'react';

import { LocaleContext } from '../../App';

type LogoProps = {
  className?: string;
  width?: number;
  height?: number;
};

const block = cn('Logo');

export const Logo: React.FC<LogoProps> = ({ className, width, height }) => {
  const { i18n } = useContext(LocaleContext);
  if (width && !height) {
    // noinspection JSSuspiciousNameCombination
    height = width;
  }
  if (height && !width) {
    // noinspection JSSuspiciousNameCombination
    width = height;
  }
  return (
    <div
      className={classnames(block(), className)}
      style={{ width: `${width}px`, height: `${height}px` }}
      title={i18n('logoAltText')}
    />
  );
};
