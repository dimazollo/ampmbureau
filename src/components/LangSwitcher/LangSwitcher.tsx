import './LangSwitcher.css';

import { cn } from '@bem-react/classname';
import { classnames } from '@bem-react/classnames';
import React, { useContext } from 'react';

import { LocaleContext, LocaleContextType } from '../../App';
import { Button } from '../Button/Button';

const block = cn('LangSwitcher');

type LangSwitcherProps = {
  className?: string;
};

export const LangSwitcher: React.FC<LangSwitcherProps> = ({ className }) => {
  const context = useContext<LocaleContextType>(LocaleContext);
  const { switchLang } = context;
  const onRuButtonClick = () => switchLang('ru');
  const onEnButtonClick = () => switchLang('en');

  return (
    <div className={classnames(block(), className)}>
      <Button onClick={() => onRuButtonClick()}>ru</Button>/<Button onClick={() => onEnButtonClick()}>en</Button>
    </div>
  );
};
