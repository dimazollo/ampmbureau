import './NavBar.css';

import { cn } from '@bem-react/classname';
import React, { useContext } from 'react';

import { LocaleContext } from '../../App';
import { Button } from '../Button/Button';
import { NavLink, useLocation } from 'react-router-dom';
import { Routes } from '../../router';
import { classnames } from '@bem-react/classnames';

const block = cn('NavBar');

interface Button {
  name: string;
  href: string;
}

const initButtonsList = [
  { name: 'main', href: Routes.main },
  { name: 'work', href: Routes.work },
  { name: 'about', href: Routes.about },
  { name: 'contacts', href: Routes.contacts }
];

type NavBarProps = {
  reorganizing?: boolean;
  className?: string;
};

export const NavBar: React.FC<NavBarProps> = ({ reorganizing, className }) => {
  const { i18n } = useContext(LocaleContext);
  const location = useLocation();

  let buttons: Button[] = initButtonsList;

  if (reorganizing) {
    while (location.pathname !== initButtonsList[0].href) {
      const b = initButtonsList.shift();
      initButtonsList.push(b!);
    }
  } else {
    buttons = initButtonsList;
  }

  return (
    <nav className={classnames(block(), className)}>
      <ul className={block('List')}>
        {buttons.map(button => (
          <li key={button.name} className={block('Link')}>
            <Button theme={'link'}>
              <NavLink to={button.href} activeStyle={{ color: 'var(--color-btn-active)' }}>
                {i18n(button.name)}
              </NavLink>
            </Button>
          </li>
        ))}
      </ul>
    </nav>
  );
};
