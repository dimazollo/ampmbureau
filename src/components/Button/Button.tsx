import './Button.css';

import { cn } from '@bem-react/classname';
import { classnames } from '@bem-react/classnames';
import * as React from 'react';

const block = cn('Button');

type ButtonProps = {
  className?: string;
  onClick?: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  theme?: 'link' | 'button' | 'link-button';
};

export const Button: React.FC<ButtonProps> = ({ className, children, onClick, theme = 'link-button' }) => {
  return (
    <button className={classnames(block({ theme }), className)} onClick={onClick}>
      {children}
    </button>
  );
};
