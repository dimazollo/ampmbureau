import React, { CSSProperties, ReactNode } from 'react';

import { cn } from '@bem-react/classname';

import './TileGrid.css';
// import vdnhTile from '../../../assets/images/VDNH.jpg';
import img1 from '../../../assets/images/mainPageTiles/1_1x_h350.jpg';
import img2 from '../../../assets/images/mainPageTiles/2_1x_h350.jpg';
import img3 from '../../../assets/images/mainPageTiles/3_1x_h350.jpg';
import img4 from '../../../assets/images/mainPageTiles/4_1x_h350.jpg';
import img5 from '../../../assets/images/mainPageTiles/5_1x_h350.jpg';
import img6 from '../../../assets/images/mainPageTiles/6_1x_h350.jpg';
import img7 from '../../../assets/images/mainPageTiles/7_1x_h350.jpg';
import img8 from '../../../assets/images/mainPageTiles/8_1x_h350.jpg';
import img9 from '../../../assets/images/mainPageTiles/9_1x_h350.jpg';
import img10 from '../../../assets/images/mainPageTiles/10_1x_h350.jpg';
import img11 from '../../../assets/images/mainPageTiles/11_1x_h350.jpg';
import img12 from '../../../assets/images/mainPageTiles/12_1x_h350.jpg';
import img13 from '../../../assets/images/mainPageTiles/13_1x_h350.jpg';
import img14 from '../../../assets/images/mainPageTiles/14_1x_h350.jpg';

import { memo, randomInt } from '../../service/utils';

type TileGridProps = {
  tilesNum: number;
  style: CSSProperties;
};

type VisibleLink = {
  img: any;
  link: string;
};

const visibleLinkList: VisibleLink[] = [
  { img: img1, link: '' },
  { img: img2, link: '' },
  { img: img3, link: '' },
  { img: img4, link: '' },
  { img: img5, link: '' },
  { img: img6, link: '' },
  { img: img7, link: '' },
  { img: img8, link: '' },
  { img: img9, link: '' },
  { img: img10, link: '' },
  { img: img11, link: '' },
  { img: img12, link: '' },
  { img: img13, link: '' },
  { img: img14, link: '' }
];

const randomTiles = memo(
  (number: number): Array<ReactNode> => {
    const result: Array<ReactNode> = [];
    for (let i = 0; i < number; i++) {
      const randomVisibleLink = visibleLinkList[randomInt(visibleLinkList.length)];
      result.push(
        // <a href={randomVisibleLink.link}>
        <img key={i} className={block('Tile')} src={randomVisibleLink.img} />
        // </a>
      );
    }
    return result;
  }
);

const block = cn('TileGrid');
export const TileGrid: React.FC<TileGridProps> = props => {
  return (
    <div className={block()} style={props.style}>
      {...randomTiles(props.tilesNum)}
    </div>
  );
};
