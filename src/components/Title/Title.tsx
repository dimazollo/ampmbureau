import './Title.css';

import { cn } from '@bem-react/classname';
import { classnames } from '@bem-react/classnames';
import React, { useContext } from 'react';

import { LocaleContext } from '../../App';

const block = cn('Title');

type TitleProps = {
  className?: string;
};

export const Title: React.FC<TitleProps> = ({ className }) => {
  const { i18n } = useContext(LocaleContext);
  return <div className={classnames(block(), className)}>{i18n('title')}</div>;
};
