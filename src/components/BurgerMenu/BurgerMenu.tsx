import * as React from 'react';
import { cn } from '@bem-react/classname';

const block = cn('BurgerMenu');

import './BurgerMenu.css';
import { classnames } from '@bem-react/classnames';

type BurgerProps = {
  active: boolean;
  className?: string;
};

export const Burger: React.FC<BurgerProps> = ({ active, className }) => (
  <div className={classnames(className, block({ active }))} />
);
