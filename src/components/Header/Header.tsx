import './Header.css';

import { cn } from '@bem-react/classname';
import { classnames } from '@bem-react/classnames';
import * as React from 'react';

import { LangSwitcher } from '../LangSwitcher/LangSwitcher';
import { NavBar } from '../NavBar/NavBar';
import { Title } from '../Title/Title';
import { Burger } from '../BurgerMenu/BurgerMenu';
import { useState } from 'react';

const block = cn('Header');

type HeaderProps = {
  className?: string;
};

type HeaderState = {
  menuOpened: boolean;
};

export const Header: React.FC<HeaderProps> = ({ className }) => {
  const [{ menuOpened }, setMenuOpened] = useState<HeaderState>({ menuOpened: false });

  const renderDesktopHeader = () => {
    return (
      <div className={classnames(block({ type: 'desktop' }), className)}>
        <NavBar reorganizing={false} />
        <Title className={block('Title')} />
        <LangSwitcher />
      </div>
    );
  };

  const renderMobileHeader = (menuOpened: boolean) => {
    return (
      <div className={classnames(block({ type: 'mobile' }), className)}>
        <div className={block('Main')}>
          <div className={block('BurgerMenu')} onClick={() => setMenuOpened({ menuOpened: !menuOpened })}>
            <Burger active={menuOpened} />
          </div>
          <Title className={block('Title')} />
        </div>
        {menuOpened && <NavBar className={block('MobileNavBar')} reorganizing={true} />}
      </div>
    );
  };

  return (
    <>
      {renderDesktopHeader()}
      {renderMobileHeader(menuOpened)}
    </>
  );
};
