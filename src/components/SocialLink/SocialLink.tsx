import './SocialLink.css';

import { cn } from '@bem-react/classname';
import React, { CSSProperties } from 'react';

export type SocialLinkProps = {
  altText: string;
  imgSrc: string;
  href: string;
  customIconStyle?: CSSProperties;
};

const block = cn('SocialLink');

export const SocialLink: React.FC<SocialLinkProps> = ({ altText, imgSrc, href, customIconStyle: iconStyle }) => {
  return (
    <span className={block()}>
      <a className={block('Link')} href={href} target="_blank" rel="noopener">
        <img className={block('Icon')} src={imgSrc} alt={altText} style={iconStyle} />
      </a>
    </span>
  );
};
