export const Routes = {
  main: '/main',
  work: '/work',
  about: '/about',
  contacts: '/contacts',
  root: '/',
  noMatch: '/404'
};
