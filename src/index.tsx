// apply styles from css
import './style.css';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { combineReducers, createStore } from 'redux';

import { App } from './App';
import { Action, State } from './reducer';
import { modalReducer } from './reducer/ModalState';
import { composeWithDevTools } from 'redux-devtools-extension';

const rootReducer = combineReducers<State, Action>({
  modal: modalReducer
});

const store = createStore(rootReducer, composeWithDevTools());

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
