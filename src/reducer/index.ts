import { ModalState } from './ModalState';
import { Action as ReduxAction } from 'redux';

export type State = {
  modal: ModalState;
};

export interface Action extends ReduxAction<string> {
  type: string;
  payload: { [key: string]: any };
}
