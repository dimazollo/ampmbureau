import { Action } from './index';

export interface ModalState {
  visible: boolean;
}

const initialModalState: ModalState = {
  visible: false
};

export const modalReducer = (state: ModalState = initialModalState, action: Action) => {
  switch (action.type) {
    default:
      return state;
  }
};
