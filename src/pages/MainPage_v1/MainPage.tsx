import './MainPage.css';

import { cn } from '@bem-react/classname';
import { classnames } from '@bem-react/classnames';
import React, { useContext } from 'react';

import { LocaleContext } from '../../App';
import { Button } from '../../components/Button/Button';
import { Logo } from '../../components/Logo/Logo';

const block = cn('MainPage');

type MainPageProps = {
  className?: string;
};

export const MainPage: React.FC<MainPageProps> = props => {
  const { i18n } = useContext(LocaleContext);

  function renderText(color: 'white' | 'black') {
    return <div className={block('TextContent', { color }, ['shiftDown'])}>{i18n('lorem')}</div>;
  }

  return (
    <div className={classnames(block(), props.className)}>
      <div className={block('Image')} title={i18n('interiorExample')}>
        <div className={block('Inner')}>
          <Logo className={block('Logo')} />
          {renderText('white')}
        </div>
      </div>
      <div className={block('Wrapper')}>
        <div className={block('Inner')}>
          <div className={block('Section')}>{renderText('black')}</div>

          <div className={block('Section')}>
            <Button theme={'button'} className={block('ServicesButton')}>
              {i18n('services')}
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};
