import './MainPage.css';

import { cn } from '@bem-react/classname';
import { classnames } from '@bem-react/classnames';
import React from 'react';

import { Logo } from '../../components/Logo/Logo';
import { TileGrid } from '../../components/TileGrid/TileGrid';

const block = cn('MainPage');

type MainPageProps = {
  className?: string;
};
type MainPageState = {
  height: number;
  width: number;
};
export class MainPage extends React.Component<MainPageProps, MainPageState> {
  private containerRef: HTMLDivElement | null = null;
  // >= 1600
  private static TILE_CONFIG_XL = {
    tileSize: 350,
    hGap: 108,
    vGap: 20
  };
  // >= 1280
  private static TILE_CONFIG_L = {
    tileSize: 300,
    hGap: 44,
    vGap: 20
  };
  // >= 980 && < 1280
  private static TILE_CONFIG_M = {
    tileSize: 270,
    hGap: 44,
    vGap: 20
  };

  // < 980
  private static TILE_CONFIG_S = {
    tileSize: 216,
    hGap: 20,
    vGap: 12
  };

  private static MAX_WIDTH_S = 980;
  private static MAX_WIDTH_M = 1280;
  private static MAX_WIDTH_L = 1600;

  constructor(props: MainPageProps) {
    super(props);

    this.state = {
      height: window.innerHeight,
      width: window.innerWidth
    };

    this.updateDimensions = this.updateDimensions.bind(this);
    this.setContainerRef = this.setContainerRef.bind(this);
  }

  private setContainerRef(element: HTMLDivElement) {
    this.containerRef = element;
  }

  private updateDimensions() {
    this.setState({
      height: this.containerRef!.offsetHeight,
      width: this.containerRef!.offsetWidth
    });
  }

  componentDidMount() {
    window.addEventListener('resize', this.updateDimensions);
    this.updateDimensions();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions);
  }

  render() {
    const { props } = this;
    const { width } = this.state;
    let config;

    if (width <= MainPage.MAX_WIDTH_S) {
      config = MainPage.TILE_CONFIG_S;
    } else if (width < MainPage.MAX_WIDTH_M) {
      config = MainPage.TILE_CONFIG_M;
    } else if (width < MainPage.MAX_WIDTH_L) {
      config = MainPage.TILE_CONFIG_L;
    } else {
      config = MainPage.TILE_CONFIG_XL;
    }

    return (
      <div className={classnames(block(), props.className)}>
        {this.renderTileGrid(config.tileSize, config.hGap, config.vGap)}

        <div ref={this.setContainerRef} className={block('Inner')}>
          <div className={block('LogoWrapper')}>
            <Logo className={block('Logo')} />
          </div>
        </div>
      </div>
    );
  }

  private renderTileGrid(tileSize: number, horizontalGap: number, verticalGap: number) {
    const containerWidth = this.state.width;
    const containerHeight = (this.containerRef && this.containerRef.offsetHeight) || 0;

    const dX = MainPage.calcShift(containerWidth, tileSize, horizontalGap);
    const dY = MainPage.calcShift(containerHeight, tileSize, verticalGap);

    const rowNum = MainPage.calcTotalTracks(containerHeight, tileSize, verticalGap);
    const colNum = MainPage.calcTotalTracks(containerWidth, tileSize, horizontalGap);

    const gridWidth = colNum * tileSize + (rowNum - 1) * horizontalGap;
    const gridHeight = rowNum * tileSize + (colNum - 1) * verticalGap;

    return (
      <div className={block('Tiles')} style={{ width: '100%', height: '100%' }}>
        <TileGrid
          tilesNum={rowNum * colNum}
          style={{
            transform: `translate(${dX}px, ${dY}px)`,
            width: `${gridWidth}px`,
            height: `${gridHeight}px`,
            gridTemplateColumns: `repeat(${colNum}, ${tileSize}px)`,
            gridTemplateRows: `repeat(auto-fit, var(--tile-height-px))`,
            gap: `${verticalGap}px ${horizontalGap}px`
          }}
        />
      </div>
    );
  }

  private static calcShift(containerSize: number, tileSize: number, gapSize: number): number {
    // width = N * tileSize + (N-1) * gapSize
    // width = N * tileSize + N * gapSize - gapSize
    // width = N * (tileSize + gapSize) - gapSize
    // N = (width + gapSize) / (tileSize + gapSize)
    const totalTracks = this.calcTotalTracks(containerSize, tileSize, gapSize);

    // const totalTracks = this.calcTotalTracks(gridSize, tileSize, gapSize);
    let tracksNum = Math.floor((totalTracks - 1) / 2);
    tracksNum = tracksNum > 0 ? tracksNum : 1;

    const shift = (containerSize - tileSize) / 2 - tracksNum * (tileSize + gapSize);
    return shift;
  }

  private static calcTotalTracks(gridSize: number, tileSize: number, gapSize: number) {
    let totalTracks = Math.floor((gridSize + gapSize) / (tileSize + gapSize));
    return totalTracks % 2 === 0 ? totalTracks + 1 : totalTracks + 2;
  }
}
