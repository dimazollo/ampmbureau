import './App.css';

import { cn } from '@bem-react/classname';
import React, { useState } from 'react';

import { Footer } from './components/Footer/Footer';
import { Header } from './components/Header/Header';
import * as translations from './i18n/translation.json';
import { MainPage } from './pages/MainPage/MainPage';
import Intl, { Locale } from './service/intl';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Routes } from './router';
// import { Work } from './pages/Work/Work';

const intl = new Intl({ dictionary: translations.keyset });

export type LocaleContextType = {
  i18n: (key: string) => string;
  switchLang: (lang: Locale) => void;
};

const initialI18n = intl.getTranslationsProcessor();
export const LocaleContext = React.createContext<LocaleContextType>({
  i18n: initialI18n,
  switchLang: () => {}
});

type AppState = { i18n: (key: string) => string };

const block = cn('App');

export const App: React.FC<{}> = () => {
  const [appState, setAppState] = useState<AppState>({ i18n: initialI18n });
  const switchLang = (lang: Locale) => {
    setAppState({ i18n: intl.getTranslationsProcessor(lang) });
  };

  return (
    <Router>
      <LocaleContext.Provider value={{ i18n: appState.i18n, switchLang }}>
        <div lang={intl.getCurrentLanguage()} className={block()}>
          <header className={block('Header')}>
            <div className={block('Inner')}>
              <Header className={block('Shrink')} />
            </div>
          </header>
          <main className={block('Main')}>
            <Switch>
              <Route path={Routes.about}>about</Route>
              <Route path={Routes.work}>{/* <Work /> */}</Route>
              <Route path={Routes.contacts}>contacts</Route>
              <Route path={Routes.main}>
                <MainPage />
              </Route>
              {/*<Route path={Routes.root}>*/}
              {/*  <Work />*/}
              {/*</Route>*/}
              <Route path={Routes.noMatch}>Страница не существует</Route>
            </Switch>
          </main>
          <footer className={block('Footer')}>
            <div className={block('Inner')}>
              <Footer className={block('Shrink')} />
            </div>
          </footer>
        </div>
      </LocaleContext.Provider>
    </Router>
  );
};
