export const scrollToTop = () => {
  window.scroll({ behavior: 'smooth', top: 0 });
};
