export type Locale = 'ru' | 'en';

type IntlParams = {
  fallbackLanguage?: Locale;
  defaultLanguage?: Locale;
  dictionary: Dictionary;
};

type Dictionary = {
  [key: string]: { [K in Locale]: string };
};

type LocaleKeyset = {
  [key: string]: string;
};

export default class Intl {
  private static LOCAL_STORAGE_LANG = 'lang';

  private readonly fallbackLanguage: Locale = 'ru';
  private readonly defaultLanguage: Locale;
  private readonly dictionary: Dictionary;

  private translationsProcessor?: (key: string) => string;
  private lastLanguage?: Locale;

  /**
   * Создает объект, управляющий инициализацией.
   * @param params - объект, в который передаются
   * словарь с переводамии, основной язык и
   * альтернативный язык, выбираемый,
   * если не окажется поддержки основного
   */
  constructor(params: IntlParams) {
    if (params.fallbackLanguage) this.fallbackLanguage = params.fallbackLanguage;
    if (params.defaultLanguage) {
      this.defaultLanguage = params.defaultLanguage;
    } else {
      this.defaultLanguage = (localStorage.getItem(Intl.LOCAL_STORAGE_LANG || navigator.language) as Locale) || 'ru';
    }
    this.dictionary = params.dictionary;
  }

  /**
   * Возвращает текущий выбранный язык
   */
  public getCurrentLanguage(): Locale {
    return this.lastLanguage || this.defaultLanguage;
  }

  /**
   * Создает функцию, которая по ключу перевода возвращает значение для текущей локали
   * @param language
   */
  public getTranslationsProcessor(language?: Locale): (key: string) => string {
    if (this.lastLanguage === language && this.translationsProcessor) {
      return this.translationsProcessor;
    }

    let localeKeyset: LocaleKeyset;
    if (!language) {
      try {
        localeKeyset = this.extractTranslations(this.defaultLanguage);
      } catch (e) {
        console.error(e.message);
        localeKeyset = this.extractTranslations(this.fallbackLanguage);
      }
    } else {
      localeKeyset = this.extractTranslations(language);
    }

    const translationsProcessor = (key: string) => {
      const value = localeKeyset[key];
      if (value === undefined) throw new Error(`Unknown key: "${key}"`);
      return value;
    };

    this.translationsProcessor = translationsProcessor;

    return translationsProcessor;
  }

  /**
   * Переключает язык, сохраняет его в локал сторадж
   * @param language
   */
  private setLanguage(language: Locale) {
    this.lastLanguage = language;
    localStorage.setItem(Intl.LOCAL_STORAGE_LANG, language);
  }

  /**
   * Достает переводы из словаря ключей и переводов для разных языков
   * @param language
   */
  private extractTranslations(language: Locale): LocaleKeyset {
    this.setLanguage(language);

    const localeKeyset: LocaleKeyset = {};
    Object.keys(this.dictionary).forEach(key => {
      const value = this.dictionary[key][language];
      if (value === undefined) {
        throw new Error(`No translation in "${language}" specified for key: "${key}"`);
      }
      localeKeyset[key] = value;
    });
    return localeKeyset;
  }
}
