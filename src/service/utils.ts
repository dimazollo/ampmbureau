/**
 * Возвращает мемоизированную обертку над переданной функцией.
 * Корректная мемоизация аргументов типа 'function' не гарантирована.
 * @param func
 */
export function memo<F extends (...args: any[]) => any>(func: F) {
  const hash = new Map<string, ReturnType<F>>();

  return (...args: Parameters<F>) => {
    const id = hashCode(JSON.stringify(args));
    if (hash.has(id)) {
      return hash.get(id);
    }
    const result = func(...args);
    hash.set(id, result);
    return result;
  };
}

export function hashCode(str: string): string {
  const radix = 16;
  let hash = 0;
  if (str.length == 0) {
    return hash.toString(radix);
  }
  for (let i = 0; i < str.length; i++) {
    const char = str.charCodeAt(i);
    hash = (hash << 5) - hash + char;
    hash = hash & hash; // Convert to 32bit integer
  }
  return hash.toString(radix);
}

export function randomColor(): string {
  const rand = () => randomInt(256);
  return `rgba(${rand()}, ${rand()}, ${rand()}, 0.5)`;
}

export function randomInt(upTo: number) {
  return Math.floor(Math.random() * upTo);
}
