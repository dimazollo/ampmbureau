require('dotenv').config();
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const config = {
  entry: {
    main: './src/index.tsx'
  },
  output: {
    publicPath: '/',
    path: path.resolve(__dirname, 'build'),
    filename: 'main.js'
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js']
  },
  devtool: 'cheap-module-eval-source-map', // can be replaced with plugin with more options
  devServer: {
    hot: true,
    historyApiFallback: true
  },
  module: {
    rules: [
      // TypeScript
      {
        exclude: /node_modules/,
        test: /\.tsx?$/,
        use: 'ts-loader'
      },
      // Style
      {
        exclude: /node_modules/,
        test: /\.css$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader', options: { importLoaders: 1 } },
          { loader: 'postcss-loader' }
        ]
      },
      // Assets
      // Images
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          {
            loader: 'url-loader',
            options: { limit: false, fallback: require.resolve('file-loader') }
          }
        ]
      },
      // Fonts
      {
        test: /\.(woff|woff2)$/,
        use: ['file-loader']
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      title: 'ampmbureau',
      hash: true,
      // todo -- add favicon
      favicon: '',
      meta: [
        {
          name: 'viewport',
          content: 'width=device-width, initial-scale=1.0, maximum-scale=2.0, minimum-scale=1.0'
        }
      ],
      template: './static/index.html'
    })
  ]
};

module.exports = (environment, argv) => {
  if (argv.mode === 'production') {
    config.output.publicPath = './';
    config.devtool = 'none';
  }

  return config;
};
